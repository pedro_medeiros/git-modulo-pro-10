package services;


import controller.vos.NewPointEntryVO;
import model.Entrytype;
import model.PointEntry;
import model.User;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import repositories.PointEntryRepository;

import java.time.LocalDateTime;

@Service
public class PointEntryService {

    private final PointEntryRepository pointEntryRepository;
    private final UserService userService;

    public PointEntryService(PointEntryRepository pointEntryRepository, UserService userService) {
        this.pointEntryRepository = pointEntryRepository;
        this.userService = userService;
    }
    public Mono<PointEntry> newEntry(final NewPointEntryVO newPointEntryVO){
        return this.userService.findById(newPointEntryVO.getUserId())
                .flatMap(user -> pointEntryRepository.save(mapToPointEntry(user, newPointEntryVO)));
    }
    public Flux<PointEntry> findByUser(final String userId){
        return this.pointEntryRepository.findByUserId(userId);
    }
    private PointEntry mapToPointEntry(User user, NewPointEntryVO newPointEntryVO){
        PointEntry pointEntry = new PointEntry();
        pointEntry.setUser(user);
        pointEntry.setType(newPointEntryVO.getType());
        pointEntry.setValue(newPointEntryVO.getValue());
        pointEntry.setEntryDate(LocalDateTime.now());
        return pointEntry;
    }

}
