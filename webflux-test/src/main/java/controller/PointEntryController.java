package controller;


import controller.vos.NewPointEntryVO;
import model.PointEntry;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import services.PointEntryService;

@RestController
@RequestMapping("/points/entry")
public class PointEntryController {

    private final PointEntryService pointEntryService;

    public PointEntryController(PointEntryService pointEntryService) {
        this.pointEntryService = pointEntryService;
    }

    @GetMapping("/userId")
    public Flux<PointEntry> findByUser(@PathVariable("userId") final String userId){
        return this.pointEntryService.findByUser(userId);
    }
    @PostMapping
    public Mono<PointEntry> newEntry(@RequestBody final NewPointEntryVO newPointEntryVO){
        return this.pointEntryService.newEntry(newPointEntryVO);
    }
}
