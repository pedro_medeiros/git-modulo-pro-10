package controller.vos;

import model.Entrytype;

import java.io.Serializable;

public class NewPointEntryVO implements Serializable {
    private String UserId;
    private Entrytype type;
    private long value;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public Entrytype getType() {
        return type;
    }

    public void setType(Entrytype type) {
        this.type = type;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "NewPointEntryVO{" +
                "UserId='" + UserId + '\'' +
                ", type=" + type +
                ", value=" + value +
                '}';
    }
}
