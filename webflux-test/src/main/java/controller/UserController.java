package controller;


import model.User;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import services.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

    private UserService userservice;

    public UserController(UserService userservice) {
        this.userservice = userservice;
    }
    @GetMapping
    public Flux<User> findAll() {
        return this.userservice.findAll();
    }
    @GetMapping("/{id}")
    public Mono<User> findById(@PathVariable("id") final String id){
        return this.userservice.findById(id);
    }
    @PostMapping
    public Mono<User> save(@RequestBody final User user){
        return this.userservice.save(user);
    }
}
