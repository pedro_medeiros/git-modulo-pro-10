package model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.List;

@Document
public class User {

    @Id
    private String titulodofilme;
    private Integer nota;
    private String comentario;
    private Integer id;

    @JsonIgnore
    private List<PointEntry> entryHistory;

    public String getTitulodofilme() {
        return titulodofilme;
    }

    public void setTitulodofilme(String titulodofilme) {
        this.titulodofilme = titulodofilme;
    }

    public Integer getNota() {
        return nota;
    }

    public void setNota(Integer nota) {
        this.nota = nota;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
